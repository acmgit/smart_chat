#Command help
smart_chat.cmd_help (Enable Help) bool true
#Command all
smart_chat.cmd_all (Enable all) bool true
#Command channels
smart_chat.cmd_channels (Enable all) bool true
#Command free_channel
smart_chat.cmd_free_channel (Enable all) bool true
#Command invite
smart_chat.cmd_invite (Enable invite) bool true
#Command join
smart_chat.cmd_join (Enable join) bool true
#Command kick
smart_chat.cmd_kick (Enable kick) bool true
#Command leave
smart_chat.cmd_leave (Enable leave) bool true
#Command list
smart_chat.cmd_list (Enable list) bool true
#Command move
smart_chat.cmd_move (Enable move) bool true
#Command status
smart_chat.cmd_status (Enable Status for Chat) bool true
#Command store_channel
smart_chat.cmd_store_channel (Enable store_channel) bool true
#Command toggle
smart_chat.cmd_toggle (Enable toggle) bool true
#Command where
smart_chat.cmd_where (Enable where) bool true

#IRC-Settings
smart_chat.irc_on (Enable IRC) bool false
smart_chat.host_ip (IP of the IRC) string localhost
smart_chat.host_port (Port of the IRC) int 6667
smart_chat.irc_channel (Channel on the IRC) string ##MT_Local
smart_chat.irc_channel_topic (Topic of the IRC-Channel) string MT_Server
smart_chat.irc_client_timeout (Set's the timeout of the IRC-Client) float 0.03
smart_chat.irc_automatic_reconnect (Reconnect on lose the connection) bool true
smart_chat.irc_automatic_reconnect_max (Max. trys to connect) int 5
smart_chat.servername (Name of the World) string Local
